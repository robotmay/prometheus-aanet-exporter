FROM ruby:3.2

WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle install

COPY . .

EXPOSE 3000

CMD ["ruby", "./prometheus-aanet-exporter"]
